/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class TriangleFrame extends JFrame {

    JLabel lblb;
    JLabel lblh;
    JTextField txtb;
    JTextField txth;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblb = new JLabel("Base:", JLabel.TRAILING);
        lblb.setSize(50, 20);
        lblb.setLocation(5, 5);
        lblb.setBackground(Color.white);
        lblb.setOpaque(true);
        this.add(lblb);

        lblh = new JLabel("Height:", JLabel.TRAILING);
        lblh.setSize(50, 20);
        lblh.setLocation(5, 40);
        lblh.setBackground(Color.white);
        lblh.setOpaque(true);
        this.add(lblh);

        txtb = new JTextField();
        txtb.setSize(50, 20);
        txtb.setLocation(60, 5);
        this.add(txtb);

        txth = new JTextField();
        txth.setSize(50, 20);
        txth.setLocation(60, 40);
        this.add(txth);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);
        this.add(btnCalculate);

        lblResult = new JLabel("Base = ? Height = ??? area = ? perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(800, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strb = txtb.getText();
                    String strh = txth.getText();
                    double b = Double.parseDouble(strb);
                    double h = Double.parseDouble(strh);
                    Triangle tri = new Triangle(b, h);
                    lblResult.setText("Triangle Base = " + String.format("%.2f", tri.getB()) + " Height =" + String.format("%.2f", tri.getH()) + " area = " + String.format("%.2f", tri.calArea()) + " perimeter = " + String.format("%.2f", tri.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txtb.setText("");
                    txtb.requestFocus();
                    txth.setText("");
                    txth.requestFocus();
                }
            }
        });

    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }

}
