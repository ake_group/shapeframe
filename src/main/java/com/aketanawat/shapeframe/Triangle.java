/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeframe;

/**
 *
 * @author Acer
 */
public class Triangle extends Shape {

    private double b;
    private double h;

    public Triangle(double b, double h) {
        super("Triangle");
        this.b = b;
        this.h = h;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }
    

    @Override
    public double calArea() {
        return b*h/2;
    }

    @Override
    public double calPerimeter() {
        return 2*h+b;
    }

}
