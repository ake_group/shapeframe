/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class RectangleFrame extends JFrame {

    JLabel lblw;
    JLabel lbll;
    JTextField txtw;
    JTextField txtl;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(800, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblw = new JLabel("Wide:", JLabel.TRAILING);
        lblw.setSize(50, 20);
        lblw.setLocation(5, 5);
        lblw.setBackground(Color.white);
        lblw.setOpaque(true);
        this.add(lblw);

        lbll = new JLabel("Long:", JLabel.TRAILING);
        lbll.setSize(50, 20);
        lbll.setLocation(5, 40);
        lbll.setBackground(Color.white);
        lbll.setOpaque(true);
        this.add(lbll);

        txtw = new JTextField();
        txtw.setSize(50, 20);
        txtw.setLocation(60, 5);
        this.add(txtw);

        txtl = new JTextField();
        txtl.setSize(50, 20);
        txtl.setLocation(60, 40);
        this.add(txtl);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);
        this.add(btnCalculate);

        lblResult = new JLabel("Wide = ? Long = ??? area = ? perimeter = ?");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(800, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strw = txtw.getText();
                    String strl = txtl.getText();
                    double w = Double.parseDouble(strw);
                    double l = Double.parseDouble(strl);
                    Rectangle re = new Rectangle(w, l);
                    lblResult.setText("Rectangle Wide = " + String.format("%.2f", re.getW()) + " Long =" + String.format("%.2f", re.getL()) + " area = " + String.format("%.2f", re.calArea()) + " perimeter = " + String.format("%.2f", re.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txtw.setText("");
                    txtw.requestFocus();
                    txtl.setText("");
                    txtl.requestFocus();
                }
            }
        });

    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
