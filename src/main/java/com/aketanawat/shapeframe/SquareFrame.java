/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class SquareFrame extends JFrame{
    JLabel lbls;
    JTextField txts;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(350, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lbls = new JLabel("Side:", JLabel.TRAILING);
        lbls.setSize(50, 20);
        lbls.setLocation(5, 5);
        lbls.setBackground(Color.white);
        lbls.setOpaque(true);
        this.add(lbls);

        txts = new JTextField();
        txts .setSize(50, 20);
        txts .setLocation(60, 5);
        this.add(txts);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Square side = ???? area = ???? perimeter = ????");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strs = txts.getText();
                    double s = Double.parseDouble(strs);
                    Square sq = new Square(s);
                    lblResult.setText("Square side = " + String.format("%.2f", sq.getS()) + " area = " + String.format("%.2f", sq.calArea()) + " perimeter = " + String.format("%.2f", sq.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txts.setText("");
                    txts.requestFocus();
                }
            }
        });
        
    }
    
    
    
    
    
    
    
    
    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
